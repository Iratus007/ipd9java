/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package peoplefromfile;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

//    *********************** created Person Class in Package 
   // class Person{
    
   // Person(String n, int a){

      // name = n;
      // age = a;
    
    //}
    
   // String name;
   // int age;
   // }
public class PeopleFromFile {

    static ArrayList<Person> people = new ArrayList<>();
    private static final String PEOPLETXT = "people.txt";
    
    
    public static void main(String[] args) {
       
        
        try{
            Scanner read = new Scanner(new FileReader(PEOPLETXT));// reads file
            
            while (read.hasNextLine()){// loop to read entire documents
                String name = read.nextLine(); //will place string in name
                int age = read.nextInt();//will parse string to int if 
                //string contains possible int value
                read.nextLine();
                people.add(new Person(name, age));// people object calls add method to creat new
                // Person object containing name and age as elements.                
            }
            read.close();
        
            } catch (IOException ioe2) {
                System.out.println("SHIFT HAPPENS");// you can have many catches.
              }
        
        
        
        for (Person p : people){// for each loop to print ArrayList 
            System.out.printf("p: name=%s, age=%d\n", p.name, p.age);
        
        }
        
        
        
        
    }
    
}
