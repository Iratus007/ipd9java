package homework2peoplemenu;
// Sebastien Scullion

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Homework2PeopleMenu {
    private static final String PEOPLETXT = "people.txt";
    static Scanner in = new Scanner(System.in);

    public static void menu() throws IOException {
        while (true) {
            System.out.println("1. Add person info\n"
                    + "2. List all persons info\n"
                    + "3. Find a person by name\n"
                    + "4. Find all persons younger than age\n"
                    + "0. Exit\n");
            System.out.print("Choice: ");
            int choice = in.nextInt();
            in.nextLine(); //consume

            if ((choice < 0) || (choice > 4)) {
                System.out.println("Choose between 1 to 4 or ZERO to exit");
                continue;
            }
            if (choice == 0) {
                System.out.println("Have a Nice day :D");
                return;
            }

            switch (choice) {
               // case 0:
                    //exit
                 //   break;
                case 1:
                    getPerson();
                    break;
                case 2:
                    list();
                    break;
                case 3:
                    find();
                    break;
                case 4:
                    younger();
                    break;
                default:
                    System.err.println("Fatal error: invalid control flow: SHIFT HAPPENS");
                    System.exit(1);
            }//end swich

        }//end while loop
    }//end menu method

    static String getName(String message) {

        System.out.println(message);
        String name = in.nextLine();
        in.nextLine();// consumes left-over new line charecter
        return name;
    }//end getName

    static String getCity(String message) {

        System.out.println(message);
        String city = in.nextLine();
        in.nextLine();// consumes left-over new line charecter
        return city;
    }//end getCity

    static int getAge(String message) {

        System.out.println(message);
        int age = in.nextInt();
        in.nextLine();// consumes left-over new line charecter
        return age;
    }//end get age

    static void getPerson() throws IOException {// throws will handle exception 
        // by calling function while try/catch will handle exceptions
        // within in the function
        try {
            String name = getName("Enter Name");
            int age = getAge("Enter Age");
            String city = getCity("Enter City");

            PrintWriter post = new PrintWriter(new FileWriter(PEOPLETXT, true));
            post.printf("%s\n%d\n%s\n", name, age, city);
            //post.println(age);
            //post.println(city);
            System.out.println("Person Added");
            System.out.println("hit enter to return to menu");
            post.close();
            
        } catch (InputMismatchException ime) {
            System.out.println(" Error writting to file. Terminating");
        }
    }

    static void list() throws IOException {
        try {
            Scanner listPerson = new Scanner(new File(PEOPLETXT));
            while (listPerson.hasNext()) {

                String person = listPerson.nextLine();
                System.out.println(person);

            }
            listPerson.close();
        } catch (IOException ioe2) {
            System.out.println("SHITF HAPPENS");
        }

    }
    
    static void find() throws IOException {

        BufferedReader out = new BufferedReader(new FileReader(PEOPLETXT));
        Scanner input = new Scanner(System.in);
        String subStr = "", line = "";
        System.out.print("Please enter partial name: ");
        subStr = input.nextLine();

        while ((line = out.readLine()) != null) {
            if (line.toLowerCase().contains(subStr.toLowerCase())) {
                System.out.println("Matches found :");
                System.out.println(line + " is " + out.readLine() + " from " + out.readLine());
            }
        }
    }

    static void younger() throws IOException {

        BufferedReader out = new BufferedReader(new FileReader(PEOPLETXT));
        Scanner input = new Scanner(System.in);
        String city, age, name;
        System.out.print("Enter maximum age: ");
        int ageMax = input.nextInt();

        while ((name = out.readLine()) != null) {
            age = out.readLine();
            city = out.readLine();
            if (ageMax > Integer.parseInt(age)) {
                System.out.println("Matches found :");
                System.out.println(name + " is " + age + " from " + city);
            }
        }
    }

// find a person
// find person by age
    public static void main(String[] args) throws IOException {
        
        menu();

    }//END main

}//END CLASS
