package quiz4items;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.InputMismatchException;
import java.util.Scanner;

abstract class Item implements Comparable<Item> {

    String name;
    double weight;

}

class TV extends Item {

    double diagonal;

    TV(String name, double weight, double diagonal) {
        super();
        this.diagonal = diagonal;
        this.name = name;
        this.weight = weight;
    }

    @Override
    public String toString() {

        return String.format("TV name: %s TV weighs: %f Diagonal is: %f\n", name, weight, diagonal);
    }

    @Override
    public int compareTo(Item o) {

        return this.name.compareTo(o.name);
    }

}

class Couch extends Item {

    int seats;

    Couch(String name, double weight, int seats) {
        super();
        this.seats = seats;
        this.name = name;
        this.weight = weight;
    }

    @Override
    public String toString() {

        return String.format("Couch name: %s Couch weighs: %f Number of seats: %d\n", name, weight, seats);
    }

    @Override
    public int compareTo(Item o) {

        return this.name.compareTo(o.name);
    }

}

class Bed extends Item {

    double width, height;

    Bed(String name, double weight, double width, double height) {
        super();
        this.height = height;
        this.width = width;
        this.name = name;
        this.weight = weight;

    }

    public String toString() {

        return String.format("Item name: %S Item Weight: %.2f Item height %.2f "
                + "Item width: %f\n", name, weight, height, width);
    }

    @Override
    public int compareTo(Item o) {

        return this.name.compareTo(o.name);
    }

}

class SortByWeight implements Comparator<Item> {

    @Override
    public int compare(Item i1, Item i2) {

        if (i1.weight == i2.weight) {
            return 0;
        }
        if (i1.weight > i2.weight) {
            return 1;
        } else {
            return -1;
        }

    }

}

class SortByNameAndLightest implements Comparator<Item> {

    @Override
    public int compare(Item b1, Item b2) {

        if (b1.name == b2.name) {
            if (b1.weight > b2.weight) {
                return 0;
            }
            return 0;
        }
        return 0;
    }

}

public class Quiz4Items {

    static ArrayList<Item> itemList = new ArrayList<>();
    static final String FILE_NAME = "input.txt";

    public static void main(String[] args) {

        try {
            Scanner fileInput = new Scanner(new File(FILE_NAME));
            while (fileInput.hasNextLine()) {
                try {
                    String line = fileInput.nextLine();

                    String[] data = line.split(";");

                    switch (data[0]) {
                        case "TV": {

                            String name = null;
                            name = data[1];
                            double weight = Double.parseDouble(data[2]);
                            double diagonal = Double.parseDouble(data[3]);
                            itemList.add(new TV(name, weight, diagonal));
                        }
                        break;
                        case "Couch": {

                            String name = null;
                            name = data[1];
                            double weight = Double.parseDouble(data[2]);
                            int seats = Integer.parseInt(data[3]);
                            itemList.add(new Couch(name, weight, seats));
                        }
                        break;
                        case "Bed": {

                            String name = null;
                            name = data[1];
                            double weight = Double.parseDouble(data[2]);
                            double width = Double.parseDouble(data[3]);
                            double height = Double.parseDouble(data[4]);
                            itemList.add(new Bed(name, weight, width, height));
                        }
                        break;

                        default:
                            System.out.println("Warning: invalid data in line " + line);
                    }
                } catch (NumberFormatException e) {
                    System.out.println("Warning: invalid data " + e.getMessage());
                    continue; // unnecessary in this case
                }
            }
            fileInput.close();
        } catch (IOException e) {
            System.err.println("Error reading file");
        } catch (InputMismatchException e) {
            System.err.println("Error: file contents mismatch");
        }
        
        System.out.println("====== READ & PRINT ======");
        for (Item t : itemList) {
            System.out.print(t);
        }
        System.out.println("");

        Collections.sort(itemList);
        System.out.println("====== SORTED BY NATURAL ORDER ======");
        for (Item t : itemList) {

            System.out.print(t);
        }
        System.out.println("");

        Collections.sort(itemList, new SortByWeight());
        System.out.println("====== SORTED BY Weight ======");
        for (Item t : itemList) {
            System.out.print(t);
        }
        System.out.println("");
        
        
         Collections.sort(itemList, new SortByNameAndLightest());
        System.out.println("====== SORTED BY Name/Weight ======");
        for (Item t : itemList) {
            System.out.print(t);
        }
        System.out.println("");

    }

}
