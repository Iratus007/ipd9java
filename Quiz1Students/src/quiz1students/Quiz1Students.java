/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quiz1students;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author student
 */
public class Quiz1Students {
    
    //private static final String PEOPLETXT = "people.txt";
    static Scanner in = new Scanner(System.in);
    public static final String STUDENTTXT = "student.txt";
    public static void menu() throws IOException {
        while (true) {
            System.out.println("1. Add student and thier GPA\n"
                    + "2. List all All Students and thier GPA\n"
                    + "3. Find a student whose name begins with\n"
                    + "4. Find the average GPA for all students and dispay\n"
                    + "0. Exit\n");
            System.out.print("Choice: ");
            int choice = in.nextInt();
            in.nextLine(); //consume

            if ((choice < 0) || (choice > 4)) {
                System.out.println("Choose between 1 to 4 or ZERO to exit");
                continue;
            }
            if (choice == 0) {
                System.out.println("Have a Nice day :D");
                return;
            }

            switch (choice) {
               
                case 1:
                    addStudent();
                    break;
                case 2:
                    list();
                    break;
                case 3:
                    find();
                    break;
                case 4:
                    avgGPA();
                    break;
                default:
                    System.err.println("Fatal error: invalid control flow: SHIFT HAPPENS");
                    System.exit(1);
            }//end swich

        }//end while loop
    }
    
    static String getName(String message) {

        System.out.println(message);
        String name = in.nextLine();
        in.nextLine();// consumes left-over new line charecter
        return name;
    }//end getName
    public static double getGPA(String message) {
        for (;;) {
            try {
                System.out.print(message);
                double value = in.nextDouble();
                
                in.nextLine(); // consume left-over new line character
                return value;
            } catch (InputMismatchException ime) {
                in.nextLine(); // consume the invalid input
                System.out.println("Invalid input, try again");
            }
        }
    }
    
    public static void main(String[] args) throws IOException {
        menu();
    }

    static void addStudent() throws IOException{
        
            String name = getName("Enter Name");
            double gpa = getGPA("Enter GPA");
            
            
            
        try {
            PrintWriter post = new PrintWriter(new FileWriter(STUDENTTXT, true));
            post.printf("%s\n%f.1\n", name, gpa);
            System.out.println("Student Added");
            System.out.println("hit enter to return to menu");
            post.close();
            
        } catch (InputMismatchException ime) {
            System.out.println(" Error writting to file. Terminating");
        }
    }

    static void list() throws IOException {
        try {
            Scanner listStudent = new Scanner(new File(STUDENTTXT));
            while (listStudent.hasNext()) {

                String student = listStudent.nextLine();
                System.out.printf(student);
                System.out.println();

            }
            listStudent.close();
        } catch (IOException ioe2) {
            System.out.println("SHITF HAPPENS");
        }

    }

    static void find() throws IOException {

        BufferedReader out = new BufferedReader(new FileReader(STUDENTTXT));
        Scanner input = new Scanner(System.in);
        String subStr, line;
        System.out.print("Please enter partial name: ");
        subStr = input.nextLine();

        while ((line = out.readLine()) != null) {
            if (line.toLowerCase().contains(subStr.toLowerCase())) {
                System.out.println("Matches found :");
                System.out.println(line + " has a GPA of" + out.readLine());
            }
        }
    }

    static void avgGPA() throws IOException {
        // needscanner.nextDouble
        File file = new File(STUDENTTXT);
        
        BufferedReader out = new BufferedReader(new FileReader(STUDENTTXT));
        double gpa, sum = 0;
        String parsegpa = out.readLine();
        int count =0;
        gpa = Double.parseDouble(parsegpa);
        try{
        if (file.exists()) {
                Scanner fileInput = new Scanner(file);
                while (fileInput.hasNextLine()) {
                    
                    gpa = fileInput.nextDouble();
                    sum = sum + gpa;
                    fileInput.nextLine();
                    System.out.println();  
                    count++;
                }
                System.out.println("The averagee is: "+sum/count);
                fileInput.close();
            } else {
                System.out.println("File does not exist");
            }
        } catch (IOException ex) {
            System.out.println("Error reading file");
        }
        
        
        
        
        

        
     }
    
        
}
        


    

   
