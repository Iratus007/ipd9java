/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quiz3moto;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

class InvalidInputDataException extends Exception {

    InvalidInputDataException(String message) {
        super(message);
    }
}

class Transport {

    public static final String INPUT = "input.txt";

    static Transport createFromLine(String line) throws InvalidInputDataException {
        try {
            Scanner read = new Scanner(new File(INPUT));
            line = read.nextLine();
            String[] parser = line.split(";");
            String description, plate;
            int wheelCount, maxPassengers;
            double maxspeed;
            while (read.hasNextLine()) {

                switch (parser[0]) {

                    case "Bicycle":{
                        description = parser[1];
                        wheelCount = Integer.parseInt(parser[2]);
                        maxPassengers = Integer.parseInt(parser[3]);
                    }
                        return Transport.add(new Bicycle(description, maxPassengers, maxPassengers));

                    case "Motorbike":{
                        description = parser[1];
                        wheelCount = Integer.parseInt(parser[2]);
                        maxPassengers = Integer.parseInt(parser[3]);
                        maxspeed = Double.parseDouble(parser[4]);
                        plate = parser[5];
                        
                    }
                        return Transport.add(new Motorbike(description, maxPassengers, maxPassengers));

                    case "Car":{
                        description = parser[1];
                        wheelCount = Integer.parseInt(parser[2]);
                        maxPassengers = Integer.parseInt(parser[3]);
                        maxspeed = Double.parseDouble(parser[4]);
                        plate = parser[5];
                        }
                        return Transport.add(new Car(description, maxPassengers, maxPassengers));

                    case "BUS":{
                        description = parser[1];
                        wheelCount = Integer.parseInt(parser[2]);
                        maxPassengers = Integer.parseInt(parser[3]);
                        maxspeed = Double.parseDouble(parser[4]);
                        plate = parser[5];
                        }
                        return Transport.add(new Bus(description, maxPassengers, maxPassengers));

                }
            }

        } catch (InputMismatchException | FileNotFoundException| NumberFormatException e) {
            throw new InvalidInputDataException("can not parse line: " + line);
        }
    }
}

class Bicycle extends Transport {
// add constructor with 3 parameters

    String description; // at least 2 characters long
    int wheelsCount; // 0-100
    int maxPassengers; // 0-1000

    Bicycle(String description, int wheelsCount, int maxPassengers) {

        this.description = description;
        this.wheelsCount = wheelsCount;
        this.maxPassengers = maxPassengers;

    }

    public String getDescription() {

        return description;

    }

    public void setDescription(String description) {
        if (description == null) {

            throw new IllegalArgumentException("description can not be null");
        }

        this.description = description;
    }

    public int getWheelsCount() {
        return wheelsCount;
    }

    public void setWheelsCount(int wheelsCount) {
        if (wheelsCount == 0) {

            throw new IllegalArgumentException("wheelsCount can not be 0");
        }
        this.wheelsCount = wheelsCount;
    }

    public int getMaxPassengers() {
        return maxPassengers;
    }

    public void setMaxPassengers(int maxPassengers) {
        if (maxPassengers == 0) {

            throw new IllegalArgumentException("maxPassengers can not be 0");
        }

        this.maxPassengers = maxPassengers;
    }

    @Override
    public String toString() {

        return String.format("");
    }
}

class Motorbike extends Bicycle {
// add constructor with 5 parameters

    private double maxSpeed; // 0-500
    private String plates; // 3-10 characters

    Motorbike(String description, int wheelsCount, int maxPassengers,  double maxSpeed, String plates) {
        super(description, wheelsCount, maxPassengers);
        setMaxSpeed(maxSpeed);
        setPlates(plates);

    }

    @Override
    public String toString() {

        return null;
    }

    public double getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(double maxSpeed) {
        if (maxSpeed == 0) {

            throw new IllegalArgumentException("maxSpeed can not be 0");
        }

        this.maxSpeed = maxSpeed;
    }

    public String getPlates() {
        return plates;
    }

    public void setPlates(String plates) {
        if (plates == null) {

            throw new IllegalArgumentException("plates can not be null");
        }

        this.plates = plates;
    }
}

class Car extends Motorbike {
    
    private double maxSpeed; // 0-500
    private String plates; // 3-10 characters
    
    Car (String description, int wheelsCount, int maxPassengers,  double maxSpeed, String plates) {
        super(description, wheelsCount, maxPassengers,maxSpeed, plates);
        

    }

    @Override
    public String toString() {

        return null;
    }

    
}

class Bus extends Motorbike {

    Bus (String description, int wheelsCount, int maxPassengers,  double maxSpeed, String plates) {
        super(description, wheelsCount, maxPassengers,maxSpeed, plates);
        

    }

    @Override
    public String toString() {

        return null;
    }
}

public class Quiz3Moto {

    static ArrayList<Transport> transportList = new ArrayList<>();

    public static void main(String[] args) {
        // TODO code application logic here
    }

}
