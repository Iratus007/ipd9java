package filesaveread;


import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.InputMismatchException;
import java.util.Scanner;



public class FileSaveRead {
    
    static Scanner input = new Scanner(System.in);
    
    static int inputInt(String message){
    
            System.out.println(message);
            int value = input.nextInt();
            input.nextLine();// consumes left-over new line charecter
            return value;
    }
    
  
     public static void main(String[] args) throws IOException { //The throws keyword indicates that myMethod might throw an IOException. If the method
//might throw multiple exceptions, add a list of the exceptions, separated by commas, after
//throws:
//Note
//If a method does not declare exceptions in the superclass, you cannot override it to
//declare exceptions in the subclass.        
        
        try{

            System.out.println("Enter Your name");
            String name = input.nextLine();
            
            int age = inputInt("Enter your age");
            
            System.out.println("Enter Your city");
            String city = input.nextLine();
                
            PrintWriter pw = new PrintWriter(new FileWriter("data.txt"));
            pw.printf("%s is %d years old and lives in %s", name, age, city);
            
            pw.close();
        
        
            Scanner fileInput = new Scanner (new File("data.txt"));
            String fileLine = fileInput.nextLine();
            System.out.println(" Read Line: " + fileLine);
            fileInput.close();
        
        } catch (InputMismatchException ime){
            System.out.println(" Invalid Input. Terminating");
        }
    }
    
}
