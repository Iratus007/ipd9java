package peopletypes;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

class Person {

    private String name;
    private int age;

    Person(String name, int age) {
        setName(name);
        setAge(age);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {

        if (name == null) {

            throw new IllegalArgumentException("Name can not be null" + name);
        }

        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {

        if (age < 0 | age > 150) {
            throw new IllegalArgumentException("Age must be between 0- 150" + age);

        }

        this.age = age;
    }

    @Override
    public String toString() {
        return String.format(getName() + " is " + getAge() + "\n");
    }

}

class Student extends Person {

    Student(String name, int age, String program, double gpa) {
        super(name, age);

        setProgram(program);
        setGpa(gpa);

    }

    private String program;
    private double gpa;

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        if (program == null) {

            throw new IllegalArgumentException("Program can not be null");
        }
        this.program = program;
    }

    public double getGpa() {

        return gpa;
    }

    public void setGpa(double gpa) {
        if (gpa < 1 | gpa > 4.3) {
            throw new IllegalArgumentException("GPA must be between 1.0 and 4.3 " + gpa);
        }
        this.gpa = gpa;
    }

    @Override
    public String toString() {
        return String.format("Student " + getName() + " is " + getAge() + " studying " + getProgram() + " wtih a " + getGpa() + "GPA\n");
    }

}

class Teacher extends Person {

    private String subject;
    private int yoe;

    Teacher(String name, int age, String subject, int yoe) {
        super(name, age);

        setSubject(subject);
        setYoe(yoe);

    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        if (subject == null | subject.length() < 2) {

            throw new IllegalArgumentException("Subject can not be null");
        }
        this.subject = subject;
    }

    public int getYoe() {
        return yoe;
    }

    public void setYoe(int yoe) {
        this.yoe = yoe;
    }

    @Override
    public String toString() {
        return String.format(getName() + " is " + getAge() + " Teaching "
                + getSubject() + " with " + getYoe() + " years of experience \n");
    }

}

public class PeopleTypes {

    public static final String PEOPLE = "people.txt";
    //static Scanner read = new Scanner(new File(PEOPLE)).useDelimiter(";");
    static ArrayList<Person> people = new ArrayList<>();

    public static void main(String[] args) {

        try {
            String name, subject;
            int age, yoe;
            double gpa;
            Scanner read = new Scanner(new File(PEOPLE));

            while (read.hasNextLine()) {

                String data = read.nextLine();
                String[] parser = data.split(";");
//                System.out.println(data);
//                System.out.println(parser[0]);
                switch (parser[0]) {

                    case "Person":
                        name = parser[1];
                        age = Integer.parseInt(parser[2]);
                        people.add(new Person(name, age));
                        break;
                    case "Teacher":
                        name = parser[1];
                        age = Integer.parseInt(parser[2]);
                        subject = parser[3];
                        yoe = Integer.parseInt(parser[4]);
                        people.add(new Teacher(name, age, subject, yoe));
                        break;
                    case "Student":
                        name = parser[1];
                        age = Integer.parseInt(parser[2]);
                        subject = parser[3];
                        gpa = Double.parseDouble(parser[4]);
                        people.add(new Student(name, age, subject, gpa));
                        break;
                }

            }

        } catch (IOException e) {
            //TODE
        }
        System.out.println("THIS IS POLYMORPHISM\n");
        for (Person a : people) {

            //System.out.print(a.toString()); is not needed, the overide is called from Object.toString which is overridden
            System.out.print(a);
        } 

        System.out.println("This is an example of casting \"Teacher t = (Teacher) p;\"\n");// This is an example of casting "Teacher t = (Teacher) p;" 
        for (Person p : people) {
            if (p instanceof Teacher) {
                Teacher t = (Teacher) p;
                System.out.printf("Teacher %s, %d y/o teaches %s with %d yoe\n", t.getName(), t.getAge(), t.getSubject(), t.getYoe());
            } else if (p instanceof Student) {
                Student s = (Student) p;
                System.out.printf("Student %s, %d y/o in %s has a GPS of %f\n"
                        , s.getName(), s.getAge(), s.getProgram(), s.getGpa());
            } else if (p instanceof Person) {
                System.out.printf("Person: %s is %d y/p\n", p.getName(), p.getAge());
            } else {
                System.out.println("ERROR"); //default

            }
        }
    }
}
