package soccerranking;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

class Team implements Comparable<Team> {

    String team;
    int matchesWon;
    int matchesLost;

    Team(String team, int matchesWon, int matchesLost) {
        this.team = team;
        this.matchesWon = matchesWon;
        this.matchesLost = matchesLost;
    }

    @Override
    public int compareTo(Team g) {
//        // compare this to g

//        if (this.matchesWon == g.matchesWon) {
//            return 0;
//        }
//        if (this.matchesWon > g.matchesWon) {
//            return 1;
//        }
//        else {
//            return -1;
//        }
//          THE ABOVE IS THE SAME AS BELOW FOR Integer
//        System.out.printf("Team: %s has won: %d lost: %d\n", team, matchesWon, matchesWon);
//      === NATURAL ORDER SORT =====
        return this.matchesWon - g.matchesWon;
    }

    @Override
    public String toString() {
        double perc = (double) matchesWon / (matchesWon + matchesLost) * 100;
        return String.format("Team: %s has won: %d lost: %d has win/loss of (%.2f%%)\n", team, matchesWon, matchesLost, perc);
    }

}

class SortTeamsByName implements Comparator<Team> {

    @Override
    public int compare(Team o1, Team o2) {

        return o1.team.compareTo(o2.team);
    }

}

class SortTeamsByPrecernt implements Comparator<Team> {

    @Override
    public int compare(Team o1, Team o2) {
        
        double perc1 = (double)o1.matchesWon / (o1.matchesWon + o1.matchesLost);
        double perc2 = (double)o2.matchesWon / (o2.matchesWon + o2.matchesLost);
        
        if (perc1 == perc2) {
            return 0;
        }
        if (perc1 > perc2) {
            return 1;
        }
        else {
            return -1;
        }
    }

}



public class SoccerRanking {

    static ArrayList<Team> teamList = new ArrayList<>();
    static final String FILE_NAME = "input.txt";

    public static void main(String[] args) {
        try {
            Scanner read = new Scanner(new File(FILE_NAME));
            while (read.hasNextLine()) {
                try {
                    String line = read.nextLine();
                    String[] data = line.split(";");
                    if (data.length != 3) {
                        throw new IllegalArgumentException("Error reading File.");
                    }
//                String teams = data[0];
//                int wins = Integer.parseInt(data[1]);
//                int losses = Integer.parseInt(data[2]);
//                
//                teamList.add(new Team(teams, wins, losses));
//               THE ABOVE IS THE SAME AS BELOW!!!!
                    Team t = new Team(data[0], Integer.parseInt(data[1]), Integer.parseInt(data[2]));
                    teamList.add(t);

                } catch (IllegalArgumentException e) {
                    System.out.println(e.getMessage());
                }

            }
        } catch (IOException e) {
            System.err.println("Error reading file");
        }

        Collections.sort(teamList);
        System.out.println("====== sorted by won order ======");
        for (Team t : teamList) {

            System.out.print(t);
        }

        Collections.sort(teamList, new SortTeamsByName());
        System.out.println("====== sorted by name ======");
        for (Team t : teamList) {

            System.out.print(t);
        }
        
        Collections.sort(teamList, new SortTeamsByPrecernt());
        System.out.println("====== sorted by %%% ======");
        for (Team t : teamList) {

            System.out.print(t);
        }
    }
    

}
