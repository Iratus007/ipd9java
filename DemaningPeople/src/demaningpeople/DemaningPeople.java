/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demaningpeople;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Sebastien
 */
public class DemaningPeople {

    private static void saveAllAirports() {
        try {
            
            
            PrintWriter pw = new PrintWriter(new FileWriter(AIRPORT));
            for (Airport a : ap){
            pw.printf("%s;%s;%f;%f;\r\n", a.getCode(), a.getCity(), a.getLat(), a.getLon());
            }
            pw.close();
        } catch (IOException ex) {
            Logger.getLogger(DemaningPeople.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    public class MinDistance {

        MinDistance(String airportCode, double distanceFromOrigin) {

            setAirportCode(airportCode);
            setDistanceFormOrigin(distanceFromOrigin);

        }
        private String airportCode;
        private double distanceFormOrigin;

        /**
         * @return the airportCode
         */
        public String getAirportCode() {
            return airportCode;
        }

        /**
         * @param airportCode the airportCode to set
         */
        public void setAirportCode(String airportCode) {
            this.airportCode = airportCode;
        }

        /**
         * @return the distanceFormOrigin
         */
        public double getDistanceFormOrigin() {
            return distanceFormOrigin;
        }

        /**
         * @param distanceFormOrigin the distanceFormOrigin to set
         */
        public void setDistanceFormOrigin(double distanceFormOrigin) {
            this.distanceFormOrigin = distanceFormOrigin;
        }
    }

    static Scanner in = new Scanner(System.in);
    public static final String AIRPORT = "airports.txt";
    static ArrayList<Airport> ap = new ArrayList<>();

    public static void main(String[] args) {

        menu();

    }

    public static void menu() {
        try {
            while (true) {
                System.out.println("1. Show all Airports\n"
                        + "2. Distance between two Airports\n"
                        + "3. Find the 2 airports nearest to an airport given and display the distance.\n"
                        + "4. Add a new airport to the list.\n"
                        + "0. Exit\n");
                System.out.print("Choice: ");

                int choice = in.nextInt();
                in.nextLine(); //consume

                if ((choice < 0) || (choice > 4)) {
                    System.out.println("Choose between 1 to 4 or ZERO to exit");
                    continue;
                }
                if (choice == 0) {
                    System.out.println("Have a Nice day :D");
                    return;
                }

                switch (choice) {

                    case 1:
                        list();
                        break;
                    case 2:
                        findDistanceBetweenTwoAirports();
                        break;
                    case 3:
                        closer();
                        break;
                    case 4:
                        newAp();
                        break;
                    default:
                        System.err.println("Fatal error: invalid control flow: SHIFT HAPPENS");
                        saveAllAirports();
                        System.exit(1);
                }//end swich

            }//end while loop
        } catch (IOException ioe2) {
            System.out.println("SHIFT HAPPENS");
        }
    }

    static void list() {
        String code;
        String city;
        double lat = 0;
        double lon = 0;
        int count = count();

        try {

            Scanner br = new Scanner(new File(AIRPORT)).useDelimiter(";");

            while (count > 0) {

                code = br.next();
                city = br.next();
                lat = br.nextDouble();
                lon = br.nextDouble();
                br.nextLine();

                //  System.out.println(code + " " + city + " " + lat + " " + lon + "Line count: " + count);
                //br.close();
                ap.add(new Airport(code, city, lat, lon));
                count--;

            }
            br.close();

        } catch (IOException ioe2) {
            System.out.println("SHIFT HAPPENS");// you can have many catches.
        }

        for (Airport a : ap) {// for each loop to print ArrayList 
            System.out.printf("a: code = %s, city = %s, lat = %f, lon = %f\n",
                    a.getCode(), a.getCity(), a.getLat(), a.getLon());
        }
    }

    static int count() {

        int count = 0;
        try {

            File file = new File(AIRPORT);

            if (file.exists()) {

                FileReader fr = new FileReader(file);
                LineNumberReader lnr = new LineNumberReader(fr);

                while (lnr.readLine() != null) {
                    count++;
                }

                //System.out.println("Total number of lines : " + count);
                lnr.close();

            } else {
                System.out.println("File does not exists!");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return count;

    }

    private static void newAp() throws IOException {
        try {
            Scanner in = new Scanner(System.in);

            System.out.println("Input Airport code");
            String code = in.nextLine();

            System.out.println("Input City Name");
            String city = in.nextLine();

            System.out.println("Enter Latitude");
            double lat = in.nextDouble();

            System.out.println("Enter Longtitude");
            double lon = in.nextDouble();
            
            Airport a = new Airport(code, city, lat, lon);
            ap.add(a);



        } catch (InputMismatchException | IllegalArgumentException ime) {
            System.err.println(" Invalid Input. Terminating"+ime.getMessage());
        }
        
    }

    // returns distance in meters - copied from
    // http://stackoverflow.com/questions/3694380/calculating-distance-between-two-points-using-latitude-longitude-what-am-i-doi
    public static double distance(double lat1, double lng1,
            double lat2, double lng2) {
        double a = (lat1 - lat2) * distPerLat(lat1);
        double b = (lng1 - lng2) * distPerLng(lat1);
        return Math.sqrt(a * a + b * b);
    }

    private static double distPerLng(double lat) {
        return 0.0003121092 * Math.pow(lat, 4)
                + 0.0101182384 * Math.pow(lat, 3)
                - 17.2385140059 * lat * lat
                + 5.5485277537 * lat + 111301.967182595;
    }

    private static double distPerLat(double lat) {
        return -0.000000487305676 * Math.pow(lat, 4)
                - 0.0033668574 * Math.pow(lat, 3)
                + 0.4601181791 * lat * lat
                - 1.4558127346 * lat + 110579.25662316;
    }

    private static double getDistanceInKm(Airport a1, Airport a2) {
        return distance(a1.getLat(), a1.getLon(),
                a2.getLat(), a2.getLon()) / 1000;
    }

    private static void findDistanceBetweenTwoAirports() {
        System.out.print("Enter airport 1 code: ");
        //
        String code1 = in.nextLine();
        Airport a1 = findAirportByCode(code1);
        if (a1 == null) {
            System.out.println("Airport not found, try again.");
            return;
        }
        //
        System.out.print("Enter airport 2 code: ");
        String code2 = in.nextLine();
        Airport a2 = findAirportByCode(code2);
        if (a2 == null) {
            System.out.println("Airport not found, try again.");
            return;
        }
        //
        System.out.printf("Distance between %s-%s and %s-%s is %.3f\n",
                a1.getCode(), a1.getCity(), a2.getCode(), a2.getCity(),
                getDistanceInKm(a1, a2));
    }

    private static Airport findAirportByCode(String code) {
        for (Airport a : ap) {
            if (a.getCode().equals(code)) {
                return a;
            }
        }
        return null;
    }

    public static void closer() {
        
        System.out.print("Enter airport code: ");
        //
        String code = in.nextLine();
        Airport a = findAirportByCode(code);
        if (a == null) {
            System.out.println("Airport not found, try again.");
            return;
        }

        System.out.printf("City: %s Lat: %.7f Lon: %.7f", a.getCode(), a.getLat(), a.getLon());
        System.out.println();
        
        
        double min2 = 100000, min = 100000;
        String printCode = null, printCode2 = null;
        double dis = 0;
        String shortCode = null;
        for (Airport compare: ap ){
            
            dis = getDistanceInKm(a, compare);
            shortCode = compare.getCode();
           if(min == 0 ){
                
            }else if (min > dis && dis != 0 ){
                min = dis;
                printCode = shortCode;
            }else if (min2 > min && min2 < dis && dis != 0){
                min2 = dis;
                printCode2 = shortCode;
            } 
        }   

            //System.out.printf("Code : %s  is %f KM away from %s", printCode , min, code);
           // System.out.printf("Code : %s  is %f KM away from %s", printCode2 , min2, code);

        System.out.printf("%s is %.2f awaya from %s\n", shortCode, min, code);
       // System.out.printf("Code: %s is cloest to %s, the distance is %.2f KM", a.getCode(), shortCode, min);
        

    }

}

