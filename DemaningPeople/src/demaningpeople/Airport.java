/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demaningpeople;

/**
 *
 * @author student
 */
public class Airport {
    
    Airport(String code, String city, double lat, double lon){
        
       setCode(code);
       setCity(city);
       setLat(lat);
       setLon(lon);
              
       
    }
    
    private String code, city;
    private double lat, lon;

    public void setCode(String code) {
        if(code ==null || code.equals("")){//code.isEmpty()
            throw new  IllegalArgumentException("Airport code must not be empty");
        }
        this.code = code;
    }

    public void setCity(String city) {
        if(city ==null || city.equals("")){//city.isEmpty()
            throw new  IllegalArgumentException("City must not be empty");
        }
        this.city = city;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }
    
    public  String getCode(){
        return code;
    }
    
    public  String getCity(){
        return city;
    }
    
    public  double getLat(){
        return lat;
    }
    
    public  double getLon(){
        return lon;
    }
    
    
    
    
    
}
