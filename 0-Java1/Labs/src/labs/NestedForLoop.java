/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labs;

/**
 *
 * @author student
 */
public class NestedForLoop {

    public static void main(String[] args) {

        int[][] sumArray = new int[4][4];

        for (int row = 0; row < sumArray.length; row++) {
            for (int column = 0; column < sumArray[row].length; column++) {
                sumArray[row][column] = (int) (Math.random() * 10);
                System.out.print(sumArray[row][column] + " ");

            }

            System.out.println();
        }
        for (int column = 0; column < sumArray[0].length; column++) {
            int total = 0;

            for (int row = 0; row < sumArray.length; row++) {
                total += sumArray[row][column];
                

            }
            System.out.println(" sum for column " + column + "is " + total);
        }

    }

}
