 
package labs;

import java.io.File;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 *
 * @author student
 */
public class fileIO04_Q2 {
    
    public static void main(String[] args){
    //H:\SebastienS\JAVA\Labs\src\labs
    
    
        // creates object named myFile 
        File myFile = new File("myintegers.txt");
        int[] number = new int[10];
        Scanner in = new Scanner(System.in);
        
        // writting to file
        try {
        
            PrintWriter fileOut = new PrintWriter(myFile);
        
            for (int i = 0; i < number.length; i++){
            
                System.out.println("Enter an Interger");
                number[i] = in.nextInt();
                fileOut.println(number[i]);
            }
            
            fileOut.close();
        
        }
        // Eception Handling
        catch (Exception e){
            
            e.printStackTrace();
        }
    
    }
}