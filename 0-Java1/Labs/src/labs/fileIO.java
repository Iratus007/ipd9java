/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labs;
import java.io.File;
import java.io.PrintWriter;
/**
 *
 * @author student
 */
public class fileIO {
    
    public static void main(String[] args){
    //H:\SebastienS\JAVA\Labs\src\labs
    
    
        // creates object named myFile 
        File myFile = new File("mytextfile.txt");
        // Eception Handling
        try {
        
            PrintWriter fileOut = new PrintWriter(myFile);
        
            
            fileOut.println(" Sebastien Scullion");
            fileOut.println("Lorena Gamarra");
            fileOut.println("Matthew Stinis");
            
            fileOut.close();
        
        }
        // Eception Handling
        catch (Exception e){
            
            e.printStackTrace();
            
        }
    
    }
    
}
