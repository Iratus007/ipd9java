/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment3;

import java.io.File;
import java.util.Scanner;

/**
 *
 * @author Sebastien
 */
public class Question3MyIntTotal {

    public static void main(String[] args){
    //H:\SebastienS\JAVA\Labs\src\labs
    
    
        // creates object named myFile 
        File myFile = new File("myintegers.txt");
        int number;
        int sum =0;
        
       
        
        // read to file
        try {
        
            Scanner fileIn = new Scanner(myFile);
        
            while(fileIn.hasNext()){
                number = fileIn.nextInt();
                System.out.println(number);
                sum=sum+number;
                
            }
            System.out.println("The SUM is: "+sum);
            
           
            fileIn.close();
        
        }
        // Eception Handling
        catch (Exception e){
            
            e.printStackTrace();
        }
    
    }    
    
    
}
