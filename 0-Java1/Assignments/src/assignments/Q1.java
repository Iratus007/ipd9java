/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignments;
import java.util.Scanner;
//import java.util.Random;
import java.text.DecimalFormat;
/**
 *
 * @author Sebastien Scullion
 */
public class Q1 {
    
    public static void main(String[] args) {
       
        double f;
        double c;
        Scanner in = new Scanner(System.in);
        DecimalFormat df = new DecimalFormat("####0.00");

        
      System.out.println("Please enter the temperature to be converted");
      f= in.nextDouble();
      c= (f-32)*5/9;
      
      if (c<=-15){
          System.out.println("It’s " + df.format(c)+" degrees Celsius, Let’s get out of here!");
        }
         else if (c > -15 || c==0 ){
             System.out.println("It’s " +df.format(c)+" degrees Celsius, Get your boots and gloves!");
            }
            else if (c>= 0 || c==15){
                System.out.println("It’s " +df.format(c)+" degrees Celsius, I have my sweater!");
               }
            else{
                 System.out.println("It’s " +df.format(c)+" degrees Celsius,It is BBQ time!");
               }
        
    }
}