/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment3;

import java.io.File;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 *
 * @author Sebastien
 */
public class Question2MyIntTXT {
    
    public static void main(String[] args){
    
         // creates object named myFile 
        File myFile = new File("myintegers.txt");
        int[] number = new int[10];
        Scanner in = new Scanner(System.in);
        
        // writting to file
        try {
        
            PrintWriter fileOut = new PrintWriter(myFile);
        
            for (int i = 0; i < number.length; i++){
            
                System.out.println("Enter an Interger");
                number[i] = in.nextInt();
                fileOut.println(number[i]);
            }
            
            fileOut.close();
        
        }
        // Eception Handling
        catch (Exception e){
            
            e.printStackTrace();
        }
        
    }
    
}
