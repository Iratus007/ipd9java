
import java.util.Random;
import java.io.File;
import java.io.PrintWriter;
import java.util.Scanner;


//@author Sebastein Scullion 1696338

public class Question3SebastienS {
    
    public static void main(String[] args){
    
        int[][] array = new int[6][5];
        Random in = new Random();
        File myFile = new File("myintegers.txt");
        
        
        try{
        
        PrintWriter fileOut = new PrintWriter(myFile);
            
          for (int row = 0; row < array.length; row++){
            for (int column = 0; column < array[row].length; column++){
            array[row][column] = in.nextInt(20 - 1)+1;
            fileOut.println(array[row][column] + " ");
            }
          }
          fileOut.close();
        }
        catch (Exception e){
            
            e.printStackTrace();
        }
    
    }
    
    
}
