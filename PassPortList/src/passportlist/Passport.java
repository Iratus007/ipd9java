/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package passportlist;

/**
 *
 * @author student
 */
public class Passport {

        Passport(String number, String firstName, String lastName, String address, String city, double heightCm, double weightKg, int yob) {

            setNumber(number);
            setFirstName(firstName);
            setLastName(lastName);
            setAddress(address);
            setCity(city);
            setHeightCm(heightCm);
            setWeightKg(weightKg);
            setYob(yob);
        }
        private String number; // passport number AB123456 format
        private String firstName; // year of birth - between 1900-2050
        private String lastName;
        private String address;
        private String city;
        private double heightCm;
        private double weightKg;
        private int yob;

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getFirstName() {
            return firstName;
        }

        /**
         * @param firstName the firstName to set
         */
        public void setFirstName(String firstName) {

            if (firstName == null || firstName.equals("")) {//city.isEmpty()
                throw new IllegalArgumentException("City must not be empty");
            }
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {

            if (lastName == null || lastName.equals("")) {//city.isEmpty()
                throw new IllegalArgumentException("City must not be empty");
            }
            this.lastName = lastName;
        }

        public String getAddress() {

            return address;
        }

        public void setAddress(String address) {
            if (address == null || address.equals("")) {//city.isEmpty()
                throw new IllegalArgumentException("City must not be empty");
            }
            this.address = address;
        }

        public String getCity() {

            return city;
        }

        public void setCity(String city) {
            if (city == null || city.equals("")) {//city.isEmpty()
                throw new IllegalArgumentException("City must not be empty");
            }
            this.city = city;
        }

        public double getHeightCm() {
            return heightCm;
        }

        public void setHeightCm(double heightCm) {
            if (heightCm == 0){//city.isEmpty()
                throw new IllegalArgumentException("City must not be empty");
            }
            this.heightCm = heightCm;
        }

        public double getWeightKg() {
            return weightKg;
        }

        public void setWeightKg(double weightKg) {
            if (weightKg ==0) {//city.isEmpty()
                throw new IllegalArgumentException("City must not be empty");
            }
            this.weightKg = weightKg;
        }

        public int getYob() {
            return yob;
        }

        public void setYob(int yob) {
            if (yob == 0) {//city.isEmpty()
                throw new IllegalArgumentException("City must not be empty");
            }
            this.yob = yob;
        }

    }

