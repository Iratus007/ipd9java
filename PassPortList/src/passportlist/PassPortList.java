package passportlist;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import static java.lang.System.in;
import java.util.ArrayList;
import java.util.Scanner;

public class PassPortList {
public class Passport {

        Passport(String number, String firstName, String lastName, String address, String city, double heightCm, double weightKg, int yob) {

            setNumber(number);
            setFirstName(firstName);
            setLastName(lastName);
            setAddress(address);
            setCity(city);
            setHeightCm(heightCm);
            setWeightKg(weightKg);
            setYob(yob);
        }
        private String number; // passport number AB123456 format
        private String firstName; // year of birth - between 1900-2050
        private String lastName;
        private String address;
        private String city;
        private double heightCm;
        private double weightKg;
        private int yob;

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getFirstName() {
            return firstName;
        }

        /**
         * @param firstName the firstName to set
         */
        public void setFirstName(String firstName) {

            if (firstName == null || firstName.equals("")) {//city.isEmpty()
                throw new IllegalArgumentException("City must not be empty");
            }
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {

            if (lastName == null || lastName.equals("")) {//city.isEmpty()
                throw new IllegalArgumentException("City must not be empty");
            }
            this.lastName = lastName;
        }

        public String getAddress() {

            return address;
        }

        public void setAddress(String address) {
            if (address == null || address.equals("")) {//city.isEmpty()
                throw new IllegalArgumentException("City must not be empty");
            }
            this.address = address;
        }

        public String getCity() {

            return city;
        }

        public void setCity(String city) {
            if (city == null || city.equals("")) {//city.isEmpty()
                throw new IllegalArgumentException("City must not be empty");
            }
            this.city = city;
        }

        public double getHeightCm() {
            return heightCm;
        }

        public void setHeightCm(double heightCm) {
            if (heightCm == 0){//city.isEmpty()
                throw new IllegalArgumentException("City must not be empty");
            }
            this.heightCm = heightCm;
        }

        public double getWeightKg() {
            return weightKg;
        }

        public void setWeightKg(double weightKg) {
            if (weightKg ==0) {//city.isEmpty()
                throw new IllegalArgumentException("City must not be empty");
            }
            this.weightKg = weightKg;
        }

        public int getYob() {
            return yob;
        }

        public void setYob(int yob) {
            if (yob == 0) {//city.isEmpty()
                throw new IllegalArgumentException("City must not be empty");
            }
            this.yob = yob;
        }

    }

    public static void main(String[] args) {
        menu();
    }

    static Scanner in = new Scanner(System.in);
    public static final String PASSPORTS = "passports.txt";
    static ArrayList<Passport> passList = new ArrayList<>();

    private static void menu() {
        //try {

        while (true) {
            System.out.println("1. Display all passports data\n"
                    + "2. Display all passports for people in the same city\n"
                    + "3. Find the tallest person and display their info.\n"
                    + "4. Find the lightest person and display their info\n"
                    + "5. Display all people younger than certain age\n"
                    + "6. Add person to list (in memory only)\n"
                    + "0. Save data back to file and exit.");

            System.out.print("Choice: ");

            int choice = in.nextInt();
            in.nextLine(); //consume

            if ((choice < 0) || (choice > 6)) {
                System.out.println("Choose between 1 to 6 or ZERO to exit");
                continue;
            }
            switch (choice) {
                case 0:
                    System.out.println(" TOODLES");
                    System.exit(0);
                    break;

                case 1:
                    dispaly();
                    break;
                case 2:
                    displayByCity();
                    break;
                case 3:
                      tallest();  ;
                    break;
                case 4:

                    break;
                case 5:

                    break;
                case 6:
                    addPerson();
                    break;
                default:
                    System.err.println("Fatal error: invalid control flow: SHIFT HAPPENS");
                    //saveAllAirports();
                    System.exit(1);
            }//end swich

        }//end while loop
        //} //catch (IOException ioe2) {
        //System.out.println("SHIFT HAPPENS");
        //}
    }

    static void dispaly() {
        String number; // passport number AB123456 format
        String firstName; // year of birth - between 1900-2050
        String lastName;
        String address;
        String city;
        double heightCm;
        double weightKg;
        int yob;
        int count = count();
        try {

            Scanner br = new Scanner(new File(PASSPORTS)).useDelimiter(";");
            Scanner in = new Scanner(System.in);
            while (br.hasNextLine()) {

                number = br.next();
                firstName = br.next();
                lastName = br.next();
                address = br.next();
                city = br.next();
                heightCm = br.nextDouble();
                weightKg = br.nextDouble();
                yob = br.nextInt();

                br.nextLine();

                //  System.out.println(code + " " + city + " " + lat + " " + lon + "Line count: " + count);
                //br.close();
                passList.add(new Passport(number, firstName, lastName, address, city, heightCm, weightKg, yob));
                count--;

            }
            br.close();

        } catch (IOException ioe2) {
            System.out.println("SHIFT HAPPENS");// you can have many catches.
        }

        for (Passport a : passList) {// for each loop to print ArrayList 
            System.out.printf("Passport Nuber: %s First Name: %s Last Name: %s "
                    + "Address: %s City: %s Height: %.2f in CM Weight: %.2f in KG YoB: %d\n",
                    a.getNumber(), a.getFirstName(), a.getLastName(),
                    a.getAddress(), a.getCity(), a.getHeightCm(), a.getWeightKg(), a.getYob());
        }
    }

    static int count() {

        int count = 0;
        try {

            File file = new File(PASSPORTS);

            if (file.exists()) {

                FileReader fr = new FileReader(file);
                LineNumberReader lnr = new LineNumberReader(fr);

                while (lnr.readLine() != null) {
                    count++;
                }

                //System.out.println("Total number of lines : " + count);
                lnr.close();

            } else {
                System.out.println("File does not exists!");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return count;

    }

    private static void displayByCity() {

        System.out.print("Enter City: ");
        //
        String city = in.nextLine();
        for (Passport found : passList){
        found = findPersonByCity(city);
            //if (city.equals(null)) {
                //System.out.println("city not found, try again.");
                //return;
           // } else{
            System.out.printf("Passport Nuber: %s First Name: %s Last Name: %s "
                    + "Address: %s City: %s Height: %.2f in CM Weight: %.2f in KG YoB: %d\n",
                    found.getNumber(), found.getFirstName(), found.getLastName(),
                    found.getAddress(), found.getCity(), found.getHeightCm(), found.getWeightKg(), found.getYob());
            //}
        }
            
    }

    private static Passport findPersonByCity(String city) {
        for (Passport a : passList) {
            if (a.getCity().equals(city)) {
                return a;
            }
        }
        return null;
    }

    private static void tallest() {
        
        
        double max = 0,tall= 0;
        for (Passport compare : passList){
            tall = compare.getHeightCm();
            if(max < tall){
                max =tall;
            }
            System.out.printf("%s is the tallest at %f\n",compare.getFirstName(), compare.getHeightCm());
         }
        
    }

    private static void addPerson() {
        
        
                String number = in.next();
                String firstName = in.next();
                String lastName = in.next();
                String address = in.next();
                String city = in.next();
                double heightCm = in.nextDouble();
                double weightKg = in.nextDouble();
                int yob = in.nextInt();

                br.nextLine();

                //  System.out.println(code + " " + city + " " + lat + " " + lon + "Line count: " + count);
                //br.close();
                passList.add(new Passport(number, firstName, lastName, address, city, heightCm, weightKg, yob));
    }

}
