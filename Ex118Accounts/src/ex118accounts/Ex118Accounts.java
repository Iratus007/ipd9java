package ex118accounts;
// EXCERSISE 9.7 P.361 and 11.8 P. 446

// TODO: Finish

/////////////////////////// classes////////////////
import java.util.ArrayList;

class Account {

    private String name, dateCreated;
    private int ID;
    private double balance, annualInterestRate;
    static int count;
    private ArrayList<Transaction> transactions = new ArrayList();
    
    Account(){}

    Account(String name, double balance, double annualInterestRate
                            , String dateCreated) {

        setName(name);
        setID();
        setBalance(balance);
        setAnnualInterestRate(annualInterestRate);
        setDateCreated(dateCreated);
        getMonthlyInterstRate();
        count++;
        
        
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getID() {
        return ID;
    }

    public void setID() {
        this.ID = count;
    }

    public double balance() {
        return getBalance();
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public double getBalance() {
        return balance;
    }

    public ArrayList<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(ArrayList<Transaction> transactions) {
        this.transactions = transactions;
    }

    /**
     * @return the annualInterestRate
     */
    public double getAnnualInterestRate() {
        return annualInterestRate;
    }

    /**
     * @param annualInterestRate the annualInterestRate to set
     */
    public void setAnnualInterestRate(double annualInterestRate) {
        this.annualInterestRate = annualInterestRate;
    }

    /**
     * @return the dateCreated
     */
    public String getDateCreated() {
        return dateCreated;
    }

    /**
     * @param dateCreated the dateCreated to set
     */
    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    private double getMonthlyInterstRate() {
        double monthlyInterstRate = annualInterestRate /12;
        double rate = balance * monthlyInterstRate;
        
        return rate;
     }
    

    private void  withdraw(double wd) {
        
        balance = balance - wd;
    }

    private void deposit(double dp) {
        balance = balance + dp;
    }
       class Transaction {

        private java.util.Date date;
        private char type;
        private double amount;
        private double balance;
        private String description;

        public Transaction(char type, double amount, double balance,
                String description) {
            
            setType(type);
            setAmount(amount);
            setBalance(balance);
            setDescription(description);
            
        
        }

        public java.util.Date getDate() {
            return date;
        }

        public void setDate(java.util.Date date) {
            this.date = date;
        }

        public char getType() {
            return type;
        }

        public void setType(char type) {
            this.type = type;
        }

        public double getAmount() {
            return amount;
        }

        public void setAmount(double amount) {
            this.amount = amount;
        }

        public double getBalance() {
            return balance;
        }

        public void setBalance(double balance) {
            this.balance = balance;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }

}

////////////////////////// end classes/////////////
public class Ex118Accounts {

    public static void main(String[] args) {
        
        
        
    }

}
