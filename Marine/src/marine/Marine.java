package marine;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

class Boat implements Comparable<Boat> {

    String name;
    double grossWeight;
    double length;
    int masts;

    Boat(String name, double grossWeight, double length, int masts) {
        this.name = name;
        this.grossWeight = grossWeight;
        this.length = length;
        this.masts = masts;
    }

    @Override
    public String toString() {

        return String.format("Boat name: %S Boat Weight: %.2f Boat length %.2f "
                + "Number of Masts: %d\n", name, grossWeight, length, masts);
    }

    @Override
    public int compareTo(Boat t) {

        return this.name.compareTo(t.name);

    }

}

class SortByLength implements Comparator<Boat> {

    @Override
    public int compare(Boat b1, Boat b2) {

        if (b1.length == b2.length) {
            return 0;
        }
        if (b1.length > b2.length) {
            return 1;
        } else {
            return -1;
        }

    }

}

class sortByMasts implements Comparator<Boat> {

    @Override
    public int compare(Boat b1, Boat b2) {

        if (b1.masts == b2.masts) {
            return 0;
        }
        if (b1.masts > b2.masts) {
            return 1;
        } else {
            return -1;
        }
    }

}

class sortByMastsOrByWeight implements Comparator<Boat> {

    @Override
    public int compare(Boat b1, Boat b2) {

        if (b1.masts == b2.masts) {
            if (b1.grossWeight > b2.grossWeight) {
                return 0;
            }
        }
        if (b1.masts > b2.masts) {
            return 1;
        } else {
            return -1;
        }
    }

}

public class Marine {

    static ArrayList<Boat> boatList = new ArrayList<>();
    static final String FILE_NAME = "input.txt";

    public static void main(String[] args) {

        try {
            Scanner read = new Scanner(new File(FILE_NAME));
            while (read.hasNextLine()) {
                try {
                    String line = read.nextLine();
                    String[] data = line.split(";");
                    if (data.length != 4) {
                        throw new IllegalArgumentException("Error reading File.");
                    }
//                
                    Boat b = new Boat(data[0], Double.parseDouble(data[1]),
                            Double.parseDouble(data[2]), Integer.parseInt(data[3]));
                    boatList.add(b);

                } catch (IllegalArgumentException e) {
                    System.out.println(e.getMessage());
                }

            }
        } catch (IOException e) {
            System.err.println("Error reading file");
        }

        Collections.sort(boatList);
        System.out.println("====== SORTED BY NATURAL ORDER ======");
        for (Boat t : boatList) {

            System.out.print(t);
        }
        System.out.println("");

        Collections.sort(boatList, new SortByLength());
        System.out.println("====== SORTED BY LENGTH ======");
        for (Boat t : boatList) {

            System.out.print(t);

        }
        System.out.println("");

        Collections.sort(boatList, new sortByMasts());
        System.out.println("====== sorted by Number of Masts ======");
        for (Boat t : boatList) {

            System.out.print(t);

        }
        System.out.println("");

        Collections.sort(boatList, new sortByMastsOrByWeight());
        System.out.println("====== sorted by Number of Masts or by Weight ======");
        for (Boat t : boatList) {

            System.out.print(t);

        }
    }

}
